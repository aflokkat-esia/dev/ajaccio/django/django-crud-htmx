from django.db import models


class Site(models.Model):
    name = models.CharField(max_length=30)
    longitude = models.FloatField(default=41.9655136)
    latitude = models.FloatField(default=8.8145168)

    def __str__(self) -> str:
        return f"{self.name}"
    
