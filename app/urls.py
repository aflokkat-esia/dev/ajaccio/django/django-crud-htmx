from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name="home"),
    path('map/', views.map, name="map"),
    path('test/', views.test, name="test"),
]


htmxpatterns = [
    path('create-site/', views.create_site, name="create-site"),
    path('update-site/<str:pk>/', views.update_site, name="update-site"),
    path('delete-site/<str:pk>/', views.delete_site, name="delete-site"),
    path('check-input-length/', views.check_input_length, name="check-input-length"),
]

urlpatterns += htmxpatterns